package com.yuzi.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;

import java.util.HashMap;
import java.util.Map;

public class SendCodeByMine {

    final private static Prop prop = PropKit.use("jConfig.properties");
    final private static String SmsCodeApi = prop.get("sms.SmsCodeApi");
    final private static String action = prop.get("sms.action");
    final private static String account = prop.get("sms.account");
    final private static String password = prop.get("sms.password");
    final private static String extno = prop.get("sms.extno");
    final private static String rt = prop.get("sms.rt");

    public static String sendCode(String mobile,String content){
        Map<String ,Object> paramMap = new HashMap<>();
        paramMap.put("action",action);
        paramMap.put("account",account);
        paramMap.put("password",password);
        paramMap.put("extno",extno);
        paramMap.put("rt",rt);
        paramMap.put("mobile",mobile);
        paramMap.put("content",content);

        return HttpClientUtils.doPost(SmsCodeApi,paramMap);

    }

    public static boolean sendCodeStatus(String mobile,String content){
        JSONObject json = JSONObject.parseObject(sendCode(mobile,content));
        String status = json.getString("status");
        JSONArray jsonArray = (JSONArray)json.get("list");
        JSONObject jsonObject = (JSONObject)jsonArray.get(0);
        String result = jsonObject.getString("result");
        if ("0".equals(status) && "0".equals(result)){
            return true;
        }else{
            return false;
        }
    }
}
