package com.yuzi.utils;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

public class DataUtils {

    final public static String FILE_log = "logo";
    final public static String FILE_photo = "photo";
    final public static String FILE_flash = "flash";
    final public static String FILE_config = "config";

    public static boolean isExitInteger(Integer[] ints,int i){

        for (Integer in:ints){
            if (i==in){
                return true;
            }
        }
        return false;
    }

    public static String generateRandom(int num){
        String str = String.valueOf(Math.random());
        return str.substring(str.indexOf(".")+1,num + 2);
    }

    public static int randomXX(int bound){
        java.util.Random random = new java.util.Random();
        return random.nextInt(bound);
    }

    public static String getSysName() {
        String osName = System.getProperty("os.name");
        if (Pattern.matches("Linux.*", osName)) {
            osName = "Linux";
        } else if (Pattern.matches("Windows.*", osName)) {
            osName = "Windows";
        } else if (Pattern.matches("Mac.*", osName)) {
            osName = "Mac";
        }
        return osName;
    }

    public static String getUUID(){
        return UUID.randomUUID().toString().replace("-", "");
    }

    //销售单据编号
    public static String saleOrderNO(){
        return DateUtils.getCurrentDateTime(DateUtils.DATE_TIME_STR);
    }

    public static String logoFileName(int i ,String fileName){
        String str = "";
        try{
            Thread.sleep(200);
            String suffix = fileName.substring(fileName.lastIndexOf(".")+1,fileName.length());
            str = "logo_" + i + "_" + DateUtils.getCurrentDateTime(DateUtils.DATE_TIME_STR) + "." + suffix;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return str;
    }

    public static String photoFileName(int i ,String fileName){
        String str = "";
        try{
            Thread.sleep(200);
            String suffix = fileName.substring(fileName.lastIndexOf(".")+1,fileName.length());
            str = "photo_" + i + "_" + DateUtils.getCurrentDateTime(DateUtils.DATE_TIME_STR) + "." + suffix;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return str;
    }

    public static String customizeFileName(int i ,String type ,String fileName){
        String str = "";
        try{
            Thread.sleep(200);
            String suffix = fileName.substring(fileName.lastIndexOf(".")+1,fileName.length());
            str =  type + "_" + i + "_" + DateUtils.getCurrentDateTime(DateUtils.DATE_TIME_STR) + "." + suffix;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return str;
    }

    //页面显示时null转成''
    public void conversionNull(Page<Record> pageFile){
        for(Record record : pageFile.getList()){
            Map<String, Object> map = record.getColumns();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String field = entry.getKey();
                Object value = entry.getValue();
                if(null==value){
                    record.set(field, "").toString();
                }
            }
        }
    }

    public String createSessionId(String phone ,String dateStr){
        Date date = DateUtils.stringToDate(dateStr,DateUtils.DATE_TIME_FORMAT);
        dateStr = DateUtils.dateToString(date,DateUtils.DATE_TIME_STR1);
        String sessionId = null;
        try {
            sessionId = MD5.md5(phone,dateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sessionId;
    }

    public static void main(String[] args) {
        for (int i=0;i<100;i++) {
            System.out.println(randomXX(3));
        }
    }
}
