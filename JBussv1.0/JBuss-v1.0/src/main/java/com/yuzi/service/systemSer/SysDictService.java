package com.yuzi.service.systemSer;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.yuzi.model.systemMo.SysDict;
import com.yuzi.service.BaseSer.BaseService;
import com.yuzi.utils.DealFieldIsNullUtils;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SysDictService extends BaseService {

    private SysDict sysDictDao;

    public Page<Record> findAllSysDict(int pageNumber, int pageSize,String dictNameSele,String dictSele){
        return sysDictDao.findAllSysDict(pageNumber, pageSize,dictNameSele,dictSele);
    }

    public Map<String, String> deleteSysDict(int id){
        final Map<String ,String> map = new HashMap<>();

        boolean res = Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                SysDict sysDict = sysDictDao.findById(id);
                if (sysDict != null){

                    boolean isDelete = sysDictDao.deleteById(id);
                    String pcode = sysDict.getStr("code");
                    List<SysDict> sysDictList = findSysDictByPcode(pcode);

                    boolean isDelete1;
                    if (sysDictList!= null && sysDictList.size()>0){
                        //将子code删掉
                        isDelete1 = deleteSysDictByPcode(pcode);
                    }else{
                        isDelete1 = true;
                    }
                    if (isDelete && isDelete1){
                        map.put("flag","true");
                        map.put("msg","删除成功");
                        return true;
                    }else{
                        map.put("flag","false");
                        map.put("msg","删除失败");
                        return false;
                    }
                }else {
                    map.put("flag","false");
                    map.put("msg","该数据不存在！");
                    return false;
                }
            }
        });
        return map;
    }

    public boolean deleteSysDictByPcode(String pcode){
        int affectCount = sysDictDao.deleteSysDictByPcode(pcode);
        if (affectCount>0){
            return true;
        }else {
            return false;
        }
    }

    public List<SysDict> findSysDictByPcode(String pcode){
        return sysDictDao.findSysDictByPcode(pcode);
    }

    public List<SysDict> findSysDictByPcode(String pcode ,List<String> excluCode){
        List<SysDict> sysDictList = findSysDictByPcode(pcode);
        Iterator<SysDict> iterator = sysDictList.iterator();
        while(iterator.hasNext()) {
            SysDict sysDict = iterator.next();
            String code = sysDict.getStr("code");
            if (excluCode.contains(code)){
                iterator.remove();
            }
        }
        return sysDictList;
    }




    public SysDict findAllSysDictById(Integer id){
        return sysDictDao.findAllSysDictById(id);
    }
    
    public Map<String,String> addSysDict(SysDict sysDict){
    	Map<String,String> map = new HashMap<>();
    	if(isExistCode(sysDict)) {
    		map.put("flag", "false");
    		map.put("res", "不能添加重复值");
    	}else {
            sysDict=(SysDict)DealFieldIsNullUtils.NullConvertToString(sysDict);
            int count = sysDictDao.addSysDict(sysDict);
            if(count>0) {
        		map.put("flag", "true");
            	map.put("res", "添加成功");
            }else {
        		map.put("flag", "false");
            	map.put("res", "添加失败");
            }
    	}
    	return map;
    }
    
    
    public Map<String,String> editSysDict(SysDict sysDict){
    	Map<String,String> map = new HashMap<>();
    	
    	boolean isExist = false;
    	if("0".equals(sysDict.get("pcode")))
    		isExist = false;
    	else 
    		isExist = isExistCode(sysDict);
    	
    	if(isExist) {
    		map.put("flag", "false");
    		map.put("res", "不能添加重复值");
    	}else {
    		boolean b = sysDictDao.editSysDict(sysDict);
    		if(b) {
        		map.put("flag", "true");
            	map.put("res", "修改成功");
            }else {
        		map.put("flag", "false");
            	map.put("res", "修改失败");
            }
    	}
    	return map;
    }
    
    public boolean isExistCode(SysDict sysDict){
        return sysDictDao.isExistCode(sysDict);
    }
    
}
