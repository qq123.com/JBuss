package com.yuzi.service.systemSer;

import com.yuzi.model.systemMo.SysRoleMenu;
import com.yuzi.service.BaseSer.BaseService;

public class SysRoleMenuService extends BaseService {

    private SysRoleMenu sysRoleMenuDao;

    public int deleteSysRoleMenuByBothId(int roleId,int menuId){
        return sysRoleMenuDao.deleteSysRoleMenuByBothId(roleId,menuId);
    }

    public boolean addSysRoleMenuByBothId(int roleId,int menuId){
        return sysRoleMenuDao.addSysRoleMenu(roleId,menuId);
    }
}
