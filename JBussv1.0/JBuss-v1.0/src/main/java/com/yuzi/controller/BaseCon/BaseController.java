package com.yuzi.controller.BaseCon;

import com.jfinal.core.Controller;
import com.yuzi.service.BaseSer.BaseService;

import java.lang.reflect.Field;

public abstract class BaseController extends Controller {

    public BaseController(){
        Field[] fields =this.getClass().getDeclaredFields();
        for (int i=0;i < fields.length; i++){
            Field field = fields[i];
            Class clazz = field.getType();
            if(BaseService.class.isAssignableFrom(clazz) && clazz != BaseService.class){
                try {
                    field.setAccessible(true);
                    field.set(this, BaseService.getInstance(clazz, this));

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
