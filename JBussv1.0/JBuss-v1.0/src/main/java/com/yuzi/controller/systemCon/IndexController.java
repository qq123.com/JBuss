package com.yuzi.controller.systemCon;

import com.jfinal.aop.Before;
import com.yuzi.common.Constant;
import com.yuzi.controller.BaseCon.BaseController;
import com.yuzi.interceptor.LoginSessionInterceptor;

@Before(LoginSessionInterceptor.class)
public class IndexController extends BaseController {
    public void index(){
        render("/WEB-INF/view/index.html");
    }

    public void showPage(){
        String page = getPara();
        render(Constant.PagePath.sysPage + page + ".html");
    }
}
