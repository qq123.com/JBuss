package com.yuzi.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.yuzi.common.Constant;
import com.yuzi.model.systemMo.SysUser;

public class LoginSessionInterceptor implements Interceptor {

    @Override
    public void intercept(Invocation inv) {
        //String session = SessionUtil.getUsernameFromSession(inv.getController().getRequest());
        SysUser sysUser = inv.getController().getSessionAttr(Constant.SessionKey.userKey);
        if (sysUser!=null){
            inv.invoke();
        }else {
            String requestUrl = inv.getController().getRequest().getRequestURI();
            if ("/login.html".equalsIgnoreCase(requestUrl)){
                inv.getController().render(Constant.PageUrl.loginPage);
            }else {
                inv.getController().redirect("/login_.html");
            }
        }
    }
}
