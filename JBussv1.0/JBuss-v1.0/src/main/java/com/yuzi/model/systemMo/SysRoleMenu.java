package com.yuzi.model.systemMo;

import com.jfinal.plugin.activerecord.Db;
import com.yuzi.model.BaseMo.BaseModel;

import java.util.List;

public class SysRoleMenu extends BaseModel<SysRoleMenu> {

    //添加权限
    public boolean addSysRoleMenu(Integer...ids){
        if (isExistSysRoleMenu(ids))
            return false;

        this.set("role_id",ids[0]);
        this.set("menu_id",ids[1]);
        return this.save();
    }

    //做权限判断其是否存在
    public boolean isExistSysRoleMenu(Integer...ids){
        List<SysRoleMenu> sysRoleMenuList = this.find("select * from sys_role_menu where role_id = ? and menu_id = ? ",ids[0],ids[1]);
        if (sysRoleMenuList != null && sysRoleMenuList.size() > 0)
            return true;
        else
            return false;
    }

    public int deleteSysRoleMenuByMenuId(int menuId){

        String sql = "delete from sys_role_menu where menu_id = ?";
        return Db.update(sql,menuId);
    }

    public int deleteSysRoleMenuByRoleId(int roleId){

        String sql = "delete from sys_role_menu where role_id = ?";
        return Db.update(sql,roleId);
    }

    public int deleteSysRoleMenuByBothId(int roleId,int menuId){
        String sql = "delete from sys_role_menu where role_id = ? and menu_id = ?";
        return Db.update(sql,roleId,menuId);
    }
}
