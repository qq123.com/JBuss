package com.yuzi.model.systemMo;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.yuzi.model.BaseMo.BaseModel;

import java.util.List;

public class SysUser extends BaseModel<SysUser> {

    public static final SysUser dao = new SysUser();

    public List<SysUser> findByName(String name){
        String sql = "select id,nick,username,password,createTime,status,note from sys_user where username = ?";
        return this.find(sql,name);
    }
    public List<SysUser> findAllSysUser(){
        return this.find("select id,nick from sys_user ");
    }

    public Page<SysUser> findAllSysUser(int pageNumber, int pageSize,String userSele,String userNameSele){
        String select = "select su.id,su.nick,su.username,su.createtime,su.status,su.note,sd.name,sr.name rolename ";
        StringBuffer sqlExceptSelect=new StringBuffer("from sys_user su ");
        sqlExceptSelect.append("left join sys_user_role sur on su.id = sur.user_id ");
        sqlExceptSelect.append("left join sys_role sr on sr.id = sur.role_id ");
        sqlExceptSelect.append("left join sys_dict sd on sd.pcode = 'userStatus' and su.status = sd.code WHERE 1 = 1");
        if(userSele!=null && !userSele.equals("")) {
        	sqlExceptSelect.append(" and username like CONCAT('%','"+userSele+"','%') ");
        }
        if(userNameSele!=null && !userNameSele.equals("")) {
        	sqlExceptSelect.append(" and nick like CONCAT('%','"+userNameSele+"','%') ");
        }
        sqlExceptSelect.append(" order by su.createtime desc");

        Page<SysUser> sysUserPage = this.paginate(pageNumber,pageSize,false,select,sqlExceptSelect.toString());
        return sysUserPage;
    }

    public Page<SysUser> findSysUserByroleId(int pageNumber, int pageSize,int roleid){
        String select = "select id,nick,username,createTime,status,note ";
        StringBuffer sqlExceptSelect=new StringBuffer("from sys_user su ");
        sqlExceptSelect.append("LEFT JOIN sys_user_role sur ON sur.`user_id` = su.`id` ");
        sqlExceptSelect.append("WHERE sur.`role_id` = '" + roleid + "'");
        sqlExceptSelect.append(" order by su.id desc");
        Page<SysUser> sysUserPage = this.paginate(pageNumber,pageSize,false,select,sqlExceptSelect.toString());
        return sysUserPage;
    }

    public int addSysUser(SysUser sysUser){
        int maxIndex = getMaxIndex(this);
        boolean isSuc = sysUser.set("id",maxIndex+1).save();
        if (isSuc)
            return sysUser.getInt("id");
        else
            return 0;
    }

    public Record findUserAndRoleByUserId(int userId){
        String sql = "select su.id,su.nick,su.username,su.status,su.note,sur.role_id " +
                "from sys_user su " +
                "left join sys_user_role sur on su.id = sur.user_id " +
                "where su.id = ?";
        return Db.findFirst(sql,userId);
    }

    public boolean updateSysUser(SysUser sysUser){

        return sysUser.update();
    }
    
  //批量删除用户
    public boolean batchDelSysUser(String params) {
    	String sql = "delete from sys_user where id in "+params;
    	return Db.delete(sql)>0?true:false;
    }

    //通过所有角色对应的用户(排除自己的用户)
    public List<Record> findRolesToUsers(Integer userId){
        String sql = "SELECT sr.id roleid,sr.name,su.id userid,su.username FROM sys_role sr \n" +
                "\tLEFT JOIN sys_user_role sur ON sr.id = sur.role_id \n" +
                "\tLEFT JOIN sys_user su ON sur.user_id = su.id \n" +
                "\twhere 1=1 " +
                "\tand su.id != ?" +
                "\tORDER BY sr.id ,su.id";
        return Db.find(sql,userId);
    }
}
