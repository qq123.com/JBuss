package com.yuzi.common;

import java.io.File;

public class Constant {

    public static final class PageUrl{
        public static final String loginPage = "/WEB-INF/view/system/login.html";

        public static final String indexPage = "/WEB-INF/view/system/index.html";
    }

    public static final class PagePath{
        public static final String sysPage = "/WEB-INF/view/system/";
    }

    public static final class SessionKey{
        public static final String userKey = "user";

        public static final String roleKey = "role";
    }

    //forbid to delete system config
    public static final class SysConfig{
        //超级用户
        public static final Integer sys_user_id = 1;

        //超级管理员
        public static final Integer sys_role_id = 1;

        //菜单管理
        public static final Integer[] sys_menu_id = {28,29,30,31,32,33};
    }

    //消息类型
    public static final class MesType{
        //提示
        public static final String tip = "tip";

        //公告
        public static final String notice = "notice";

        //信息
        public static final String mes = "mes";

    }

    public static final class MesStatus{
        //待发送
        public static final String toSend = "ToSend";

        //已发送
        public static final String Sended = "Sended";
    }

    //文件保存路径
    public static final class FileType{

        public static String filePath(String...strings){
            String filePath = "";
            for (String str:strings){
                filePath += str + File.separator;
            }
            return filePath;
        }

        public static final String logo = "logo";
        public static final String flash = "flash";
        public static final String config = "config";
        public static final String temp = "temp";
    }

    //缓存信息ehcache
    public static final class CacheType{

        //flash
        public static final String firstCache = "firstCache";
    }

    public static final class RedisType{

        public static final String pvCacheName = "pvCacheName";
    }

    //webSocket Callback
    public static final class SocketCall{
        //关闭浏览器
        public static final String closeBrowser = "closeBrowser";

        //关闭Session
        public static final String closeSession = "closeSession";
    }

}
