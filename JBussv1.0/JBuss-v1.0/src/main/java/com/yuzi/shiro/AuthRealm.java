package com.yuzi.shiro;

import com.jfinal.kit.StrKit;
import com.yuzi.model.systemMo.SysUser;
import com.yuzi.shiro.ext.CaptchaRender;
import com.yuzi.shiro.ext.CaptchaUsernamePasswordToken;
import com.yuzi.shiro.ext.IncorrectCaptchaException;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;

import java.util.List;

public class AuthRealm extends AuthorizingRealm {

    /**
     *授权方法
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.addStringPermission("abc");
        System.out.println("执行授权方法。。。");
        return info;
    }

    /**
     * 认证方法
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {

        CaptchaUsernamePasswordToken token = (CaptchaUsernamePasswordToken) authenticationToken;

        //验证用户名
        String username = token.getUsername();
        if (StrKit.isBlank(username)) {
            throw new AuthenticationException("用户名不可以为空");
        }
        //验证验证码
        Subject subject = SecurityUtils.getSubject();
        Session session = subject.getSession(false);
        String md5Code = null;
        if(session != null){
            md5Code = (String)session.getAttribute(CaptchaRender.DEFAULT_CAPTCHA_MD5_CODE_KEY);
        }
        boolean isRight = CaptchaRender.validate(md5Code, token.getCaptcha());
        if (!isRight) {
            throw new IncorrectCaptchaException("验证码错误!");
        }

        List<SysUser> dbUser = SysUser.dao.findByName(token.getUsername());
        if (null == dbUser || dbUser.size()==0){
            //用户不存在
            throw new UnknownAccountException("用户不存在!");
        }else if ("off".equals(dbUser.get(0).getStr("status"))){
            // 判断帐号是否锁定
            // 抛出 帐号锁定异常
            throw new LockedAccountException("该账户已经被禁用，请联系管理员！");

        }else{
                //验证密码
                String password = String.valueOf(token.getPassword());
                PasswordService svc = new DefaultPasswordService();
                if(svc.passwordsMatch(password, dbUser.get(0).get("password").toString())){
                    return  new SimpleAuthenticationInfo(dbUser.get(0).remove("password"), password, getName());
                }else{
                    throw new AuthenticationException("密码错误!");
                }
        }
    }

}
