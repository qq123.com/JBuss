﻿
JBuss快速开发：
1.在mysql中创建Jbuss数据库
2.将Jbuss-init.sql导入mysql（导入的时候应该注意一个问题：sys_menu表中有一个索引为0的数据，mysql默认情况下是从1开始自增的，所以这个0将会成为最大索引，导入后，将这个最大索引修改为0，否则项目中的菜单将无法显示）
3.将项目部署到idea开发工具中
4.发布到tomcat中启动，预览效果


JBuss快速预览：
1.在mysql中创建Jbuss数据库
2.将Jbuss-init.sql导入mysql
3.解压tomcat-Jbuss-v1.0,并启动tomca/bin/startup.bat
4.访问JBuss开发者后台（提前安装好jre8.0）
	localhost:8080/JBuss/login_
	superadmin/admin